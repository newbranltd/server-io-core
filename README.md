# server-io-core 

```sh
$ npm install server-io-core --save-dev
```

```sh
$ npm install --global server-io-core 
$ server-io-core /path/to/your/web/root

```



## Documentations 

[中文使用手册](https://gitlab.com/newbranltd/server-io-core/blob/master/_CN.md)

[English Manual](https://gitlab.com/newbranltd/server-io-core/blob/master/_EN.md)

---


Brought to you by [NEWBRAN.CH](https://newbran.ch) / [Joel Chu](https://joelchu.com)

