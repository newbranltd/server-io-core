const DEFAULT_DELAY = 1000;
const EVENT_NAME = 'change';
const DEFAULT_WAIT = 5000;

module.exports = {
  DEFAULT_DELAY,
  EVENT_NAME,
  DEFAULT_WAIT
};
